-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-11-2018 a las 21:10:17
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `beerrecipes`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hop`
--

CREATE TABLE `hop` (
  `id_hop` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `recipe_name` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `origin` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `notes` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `type` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `inventory` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `version` int(11) NOT NULL,
  `amount` double NOT NULL,
  `time` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hop`
--

INSERT INTO `hop` (`id_hop`, `name`, `recipe_name`, `origin`, `notes`, `type`, `inventory`, `version`, `amount`, `time`) VALUES
(15, 'Goldings, East Kent', 'Burton Ale', 'United Kingdom', 'Used For: General purpose hops for bittering/finishing all British Ales\nAroma: Floral, aromatic, earthy, slightly sweet spicy flavor\nSubstitutes: Fuggles, BC Goldings\nExamples: Bass Pale Ale, Fullers ESB, Samual Smith\'s Pale Ale\n', 'Aroma', '1.00 oz', 1, 0.02835, 60),
(16, 'Northern Brewer', 'Burton Ale', 'Germany', 'Also called Hallertauer Northern Brewers\nUse for: Bittering and finishing both ales and lagers of all kinds\nAroma: Fine, dry, clean bittering hop.  Unique flavor.\nSubstitute: Hallertauer Mittelfrueh, Hallertauer\nExamples: Anchor Steam, Old Peculiar, ', 'Both', '0.50 oz', 1, 0.014175, 60),
(17, 'Fuggles', 'Burton Ale', 'United Kingdom', 'Used For: General purpose bittering/aroma for English Ales, Dark Lagers\nAroma: Mild, soft, grassy, floral aroma\nSubstitute: East Kent Goldings, Williamette\nExamples: Samuel Smith\'s Pale Ale, Old Peculiar, Thomas Hardy\'s Ale', 'Aroma', '0.50 oz', 1, 0.014175, 2),
(18, 'Fuggles', 'Burton Ale', 'United Kingdom', 'Used For: General purpose bittering/aroma for English Ales, Dark Lagers\nAroma: Mild, soft, grassy, floral aroma\nSubstitute: East Kent Goldings, Williamette\nExamples: Samuel Smith\'s Pale Ale, Old Peculiar, Thomas Hardy\'s Ale', 'Aroma', '0.75 oz', 1, 0.021262, 4320),
(19, 'Goldings, East Kent', 'Dry Stout', 'United Kingdom', 'Used For: General purpose hops for bittering/finishing all British Ales\nAroma: Floral, aromatic, earthy, slightly sweet spicy flavor\nSubstitutes: Fuggles, BC Goldings\nExamples: Bass Pale Ale, Fullers ESB, Samual Smith\'s Pale Ale\n', 'Aroma', '2.25 oz', 1, 0.063786, 60),
(20, 'Fuggles', 'Porter', 'United Kingdom', 'Used For: General purpose bittering/aroma for English Ales, Dark Lagers\nAroma: Mild, soft, grassy, floral aroma\nSubstitute: East Kent Goldings, Williamette\nExamples: Samuel Smith\'s Pale Ale, Old Peculiar, Thomas Hardy\'s Ale', 'Aroma', '2.00 oz', 1, 0.056699, 60),
(21, 'Goldings, East Kent', 'Wit', 'United Kingdom', 'Used For: General purpose hops for bittering/finishing all British Ales\nAroma: Floral, aromatic, earthy, slightly sweet spicy flavor\nSubstitutes: Fuggles, BC Goldings\nExamples: Bass Pale Ale, Fullers ESB, Samual Smith\'s Pale Ale\n', 'Aroma', '1.00 oz', 1, 0.02835, 60),
(22, 'Hervir cebada', 'Mahou', 'Xàtiva', 'Hervir a fuego lento para incrementar el aroma.', 'rubia', 'Jose Francisco', 2, 2.2, 90);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recipe`
--

CREATE TABLE `recipe` (
  `name` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `version` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `batch_size` double NOT NULL,
  `boil_size` double NOT NULL,
  `efficiency` double NOT NULL,
  `boil_time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `recipe`
--

INSERT INTO `recipe` (`name`, `version`, `batch_size`, `boil_size`, `efficiency`, `boil_time`) VALUES
('Burton Ale', '1', 18.927168, 20.819885, 72, 60),
('Dry Stout', '1', 18.927168, 20.819885, 72, 60),
('Mahou', '7.5', 7, 18, 77, 120),
('Porter', '1', 18.927168, 18.927168, 72, 60),
('Wit', '1', 19.684255, 19.684255, 75, 60);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `hop`
--
ALTER TABLE `hop`
  ADD PRIMARY KEY (`id_hop`),
  ADD KEY `hop_ibfk_1` (`recipe_name`);

--
-- Indices de la tabla `recipe`
--
ALTER TABLE `recipe`
  ADD PRIMARY KEY (`name`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `hop`
--
ALTER TABLE `hop`
  MODIFY `id_hop` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `hop`
--
ALTER TABLE `hop`
  ADD CONSTRAINT `hop_ibfk_1` FOREIGN KEY (`recipe_name`) REFERENCES `recipe` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
