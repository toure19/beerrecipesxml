/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.Recipe;

/**
 *
 * @author toure19
 */
public class BeerDAO {

    public void inserta(Connection con, Recipe recipe) throws Exception {
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement("INSERT INTO recipe (name,version,batch_size,boil_size, efficiency,boil_time) VALUES(?,?,?,?,?,?)");
            stmt.setString(1, recipe.getName());
            stmt.setString(2, recipe.getVersion());
            stmt.setDouble(3, recipe.getBatchSize());
            stmt.setDouble(4, recipe.getBoilSize());
            stmt.setDouble(5, recipe.getEfficiency());
            stmt.setInt(6, recipe.getBoilTime());
            stmt.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new Exception("problemas al insertar recipe " + ex.getMessage());
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    public void elimina(Connection con, Recipe recipe) throws Exception {
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement("DELETE FROM recipe WHERE name=?");
            stmt.setString(1, recipe.getName());
            stmt.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new Exception("problemas al borrar recipe " + ex.getMessage());
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    public void actualiza(Connection con, Recipe recipe) throws Exception {
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement("UPDATE recipe SET version=?,batch_size=?, boil_size=?, efficiency=?, boil_time=? WHERE name=?");
            stmt.setString(1, recipe.getVersion());
            stmt.setDouble(2, recipe.getBatchSize());
            stmt.setDouble(3, recipe.getBoilSize());
            stmt.setDouble(4, recipe.getEfficiency());
            stmt.setInt(5, recipe.getBoilTime());
            stmt.setString(6, recipe.getName());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new Exception("Problemas al actualizar recipe " + ex.getMessage());
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    public Recipe findByName(Connection con, String name) throws Exception {
        Recipe recipe = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            stmt = con.prepareStatement("SELECT * FROM Recipe WHERE name=?");
            stmt.setString(1, name);
            rs = stmt.executeQuery();

            while (rs.next()) {
                recipe = new Recipe();
                obtenRecipeFila(rs, recipe);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new Exception("problema al buscar por name " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
        return recipe;
    }

    private void obtenRecipeFila(ResultSet rs, Recipe recipe) throws Exception {
        recipe.setName(rs.getString("name"));
        recipe.setVersion(rs.getString("version"));
        recipe.setBatchSize(rs.getDouble("batch_size"));
        recipe.setBoilSize(rs.getDouble("boil_size"));
        recipe.setEfficiency(rs.getDouble("efficiency"));
        recipe.setBoilTime(rs.getInt("boil_time"));
    }

}
