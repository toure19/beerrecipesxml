/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.Hop;

/**
 *
 * @author toure19_notebook
 */
public class HopDAO {

    public void inserta(Connection con, Hop hop) throws Exception {
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement("INSERT INTO hop (name,recipe_name,origin,notes,type, inventory,version,amount,time) VALUES(?,?,?,?,?,?,?,?,?)");
            stmt.setString(1, hop.getName());
            stmt.setString(2, hop.getRecipeName());
            stmt.setString(3, hop.getOrigin());
            stmt.setString(4, hop.getNotes());
            stmt.setString(5, hop.getType());
            stmt.setString(6, hop.getInventory());
            stmt.setInt(7, hop.getVersion());
            stmt.setDouble(8, hop.getAmount());
            stmt.setDouble(9, hop.getTime());
            stmt.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new Exception("problemas al insertar hop " + ex.getMessage());
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    public void elimina(Connection con, Hop hop) throws Exception {
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement("DELETE FROM hop WHERE name=?");
            stmt.setString(1, hop.getName());
            stmt.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new Exception("problemas al borrar hop " + ex.getMessage());
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    public void actualiza(Connection con, Hop hop) throws Exception {
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement("UPDATE hop SET recipe_name=?,origin=?,notes=?, type=?, inventory=?, version=?,amount=?,time=? WHERE name=?");
            stmt.setString(1, hop.getRecipeName());
            stmt.setString(2, hop.getOrigin());
            stmt.setString(3, hop.getNotes());
            stmt.setString(4, hop.getType());
            stmt.setString(5, hop.getInventory());
            stmt.setInt(6, hop.getVersion());
            stmt.setDouble(7, hop.getAmount());
            stmt.setDouble(8, hop.getTime());
            stmt.setString(9, hop.getName());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new Exception("Problemas al actualizar hop " + ex.getMessage());
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    public Hop findByName(Connection con, String name) throws Exception {
        Hop hop = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            stmt = con.prepareStatement("SELECT * FROM hop WHERE name=?");
            stmt.setString(1, name);
            rs = stmt.executeQuery();

            while (rs.next()) {
                hop = new Hop();
                obtenerHopFila(rs, hop);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new Exception("problema al buscar por name " + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
        return hop;
    }

    private void obtenerHopFila(ResultSet rs, Hop hop) throws Exception {
        hop.setName(rs.getString("name"));
        hop.setRecipeName(rs.getString("recipe_name"));
        hop.setOrigin(rs.getString("origin"));
        hop.setNotes(rs.getString("notes"));
        hop.setType(rs.getString("type"));
        hop.setInventory(rs.getString("inventory"));
        hop.setVersion(rs.getInt("version"));
        hop.setAmount(rs.getDouble("amount"));
        hop.setTime(rs.getDouble("time"));
    }
}
