/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.DAO;

import java.sql.Connection;
import java.sql.SQLException;
    
        
public class ConnectionDB {
    
    public Connection abrirConexion() throws SQLException, Exception {
        Connection con = null;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            String urlOdbc = "jdbc:mysql://localhost:3306/beerrecipes";
            con = (java.sql.DriverManager.getConnection(urlOdbc, "root", ""));
        } catch (Exception ex) {
            throw new Exception("No se ha podido establecer la conexión" + ex.getMessage());
        }
        return con;
    }

    public void cerrarConexion(Connection con) throws Exception {
        if (con != null) {
            try {
                con.close();
            } catch (Exception ex) {
                throw new Exception("No es posible cerrar la conexión" + ex.getMessage());
                //Logger.getLogger(Conexion_DB.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
