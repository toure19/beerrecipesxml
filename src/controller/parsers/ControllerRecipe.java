/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.parsers;

import java.util.ArrayList;
import model.Constants;
import model.Hop;
import model.Recipe;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author toure19
 */
public class ControllerRecipe extends ControllerDom {

    private ArrayList<Hop> listHop;

    public ControllerRecipe() {
    }

    public static Recipe readRecipe(Element elRecipe) {
        Recipe recipeObj = new Recipe();
        Hop hopObj = null;
        recipeObj.setName(getElementTag(Constants.NAME, elRecipe).getTextContent()); //Name
        recipeObj.setVersion(getElementTag(Constants.VERSION, elRecipe).getTextContent()); //Version
        recipeObj.setBatchSize(Double.parseDouble(getElementTag(Constants.BATCHSIZE, elRecipe).getTextContent())); //Batch size
        recipeObj.setBoilSize(Double.parseDouble(getElementTag(Constants.BOILSIZE, elRecipe).getTextContent())); //Boil size
        recipeObj.setEfficiency(Double.parseDouble(getElementTag(Constants.EFFICIENCY, elRecipe).getTextContent())); //Efficiency
        recipeObj.setBoilTime(Integer.parseInt(getElementTag(Constants.BOILTIME, elRecipe).getTextContent())); //Boil time
        NodeList listHops = getElementTag(Constants.HOPS, elRecipe).getChildNodes(); //lista de nodos hop
        for (int i = 0; i < listHops.getLength(); i++) { //Recorrer hops
            if (listHops.item(i).getNodeType() == Node.ELEMENT_NODE) {
                hopObj = new Hop();
                hopObj.setName(getElementTag(Constants.NAME, (Element) listHops.item(i)).getTextContent()); //Name
                hopObj.setRecipeName(getElementTag(Constants.NAME, elRecipe).getTextContent()); //Name
                hopObj.setOrigin(getElementTag(Constants.ORIGIN, (Element) listHops.item(i)).getTextContent()); //Origin
                hopObj.setNotes(getElementTag(Constants.NOTES, (Element) listHops.item(i)).getTextContent()); //Notes
                hopObj.setType(getElementTag(Constants.TYPE, (Element) listHops.item(i)).getTextContent()); //Type
                hopObj.setInventory(getElementTag(Constants.INVENTORY, (Element) listHops.item(i)).getTextContent()); //Inventory
                hopObj.setVersion(Integer.parseInt(getElementTag(Constants.VERSION, (Element) listHops.item(i)).getTextContent())); //Version
                hopObj.setAmount(Double.parseDouble(getElementTag(Constants.AMOUNT, (Element) listHops.item(i)).getTextContent())); //Amount
                hopObj.setTime(Double.parseDouble(getElementTag(Constants.TIME, (Element) listHops.item(i)).getTextContent())); //Time
                recipeObj.getListHop().add(hopObj); //Añadimos los hop al objeto recipeObj
            }
        }
        return recipeObj;

    }

    public void writeRecipe(Document doc, Element elRecipe, Recipe recipe) {
        Element name = doc.createElement(Constants.NAME); //Principio Name 
        name.appendChild(doc.createTextNode(recipe.getName()));
        elRecipe.appendChild(name); //Fin Name

        Element version = doc.createElement(Constants.VERSION); //Principio Version 
        version.appendChild(doc.createTextNode(recipe.getVersion()));
        elRecipe.appendChild(version); //Fin Version

        Element batchSize = doc.createElement(Constants.BATCHSIZE); //Principio BatchSize 
        batchSize.appendChild(doc.createTextNode(String.valueOf(recipe.getBatchSize())));
        elRecipe.appendChild(batchSize); //Fin BatchSize

        Element boilSize = doc.createElement(Constants.BOILSIZE); //Principio BoilSize 
        boilSize.appendChild(doc.createTextNode(String.valueOf(recipe.getBoilSize())));
        elRecipe.appendChild(boilSize); //Fin BoilSize

        Element efficiency = doc.createElement(Constants.EFFICIENCY); //Principio Efficiency 
        efficiency.appendChild(doc.createTextNode(String.valueOf(recipe.getEfficiency())));
        elRecipe.appendChild(efficiency); //Fin Efficiency

        Element boilTime = doc.createElement(Constants.BOILTIME); //Principio BoilTime 
        boilTime.appendChild(doc.createTextNode(String.valueOf(recipe.getBoilTime())));
        elRecipe.appendChild(boilTime); //Fin BoilTime

        Element hops = doc.createElement(Constants.HOPS); //Principio Hops

        for (int i = 0; i < recipe.getListHop().size(); i++) { //Recorremos todos los hops
            Element hop = doc.createElement(Constants.HOP); //Principio Hop

            Element nameHop = doc.createElement(Constants.HOP); //Principio Name Hop
            nameHop.appendChild(doc.createTextNode(recipe.getListHop().get(i).getName()));
            hop.appendChild(nameHop);//Final Name Hop

            Element recipeName = doc.createElement(Constants.RECIPENAME); //Principio Recipe Name
            recipeName.appendChild(doc.createTextNode(recipe.getName()));
            hop.appendChild(recipeName); //Final Recipe Name

            Element origin = doc.createElement(Constants.ORIGIN); //Principio Origin
            origin.appendChild(doc.createTextNode(recipe.getListHop().get(i).getOrigin()));
            hop.appendChild(origin);//Final Origin

            Element notes = doc.createElement(Constants.NOTES); //Principio Notes
            notes.appendChild(doc.createTextNode(recipe.getListHop().get(i).getNotes()));
            hop.appendChild(notes);//Final Notes

            Element type = doc.createElement(Constants.TYPE); //Principio Type
            type.appendChild(doc.createTextNode(recipe.getListHop().get(i).getType()));
            hop.appendChild(type);//Final Type

            Element inventory = doc.createElement(Constants.INVENTORY); //Principio Inventory
            inventory.appendChild(doc.createTextNode(recipe.getListHop().get(i).getInventory()));
            hop.appendChild(inventory);//Final Inventory

            Element versionHop = doc.createElement(Constants.VERSION); //Principio Version Hop
            versionHop.appendChild(doc.createTextNode(String.valueOf(recipe.getListHop().get(i).getVersion())));
            hop.appendChild(versionHop);//Final Version Hop

            Element amount = doc.createElement(Constants.AMOUNT); //Principio Amount
            amount.appendChild(doc.createTextNode(String.valueOf(recipe.getListHop().get(i).getAmount())));
            hop.appendChild(amount);//Final Amount

            Element time = doc.createElement(Constants.TIME); //Principio Time
            time.appendChild(doc.createTextNode(String.valueOf(recipe.getListHop().get(i).getTime())));
            hop.appendChild(time);//Final Time

            hops.appendChild(hop); //Fin Hop
        }
        elRecipe.appendChild(hops); //Fin Hops
    }

}
