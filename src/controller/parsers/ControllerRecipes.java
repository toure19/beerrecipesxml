/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.parsers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import model.Constants;
import model.Hop;
import model.Recipe;
import model.Recipes;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author toure19
 */
public class ControllerRecipes extends ControllerDom {

    ArrayList<Recipe> listRecipes;
    Recipes recipes = new Recipes();

    public ControllerRecipes() {
    }

    public Document recove(File xmlFile) throws ParserConfigurationException, SAXException, IOException {
        Document doc = null;
        doc = xmlToDom(doc, xmlFile);
        return doc;
    }

    public void save(Document doc, File newDestination) throws TransformerException {
        domToXml(doc, newDestination);
    }

    public Recipes readRecipes(Document doc) {
        Element root = doc.getDocumentElement();
        NodeList listRecipes = root.getChildNodes();
        for (int i = 0; i < listRecipes.getLength(); i++) {
            if (listRecipes.item(i).getNodeType() == Node.ELEMENT_NODE) {
                recipes.add(ControllerRecipe.readRecipe((Element) listRecipes.item(i)));
            }
        }
        return recipes;
    }

    public Document writeRecipes(Document doc, Recipes recipes) {
        ControllerRecipe controllerRecipe = new ControllerRecipe();
        Element root = doc.createElement(Constants.RECIPES);
        doc.appendChild(root);
        for (int i = 0; i < recipes.size(); i++) {
            Element tagRecipe = doc.createElement(Constants.RECIPE);
            controllerRecipe.writeRecipe(doc, tagRecipe, recipes.get(i));
            root.appendChild(tagRecipe);
        }
        return doc;
    }

    public Recipes createRecipesDefault(Recipes recipes) {

        //Por defecto 0
        Recipe recipe_0 = new Recipe("Socarrada", "1", 2.15, 3.3, 0.18, 60);
        Hop hop_0 = new Hop("Xàtiva, Valencia", recipe_0.getName(), "España", "Da un toque amargo a la cerveza.", "Gusto", "100cl", 2, 2.00, 60.00);
        Hop hop_1 = new Hop("Sorio, Valencia", recipe_0.getName(), "España", "Da un aroma diferente a la cerveza.", "Aroma", "200cl", 1, 22.00, 100.00);
        Hop hop_2 = new Hop("Chella, Valencia", recipe_0.getName(), "España", "Da un color más oscuro a la cerveza.", "Visual", "10cl", 3, 1.00, 55.55);
        recipe_0.getListHop().add(hop_0);
        recipe_0.getListHop().add(hop_1);
        recipe_0.getListHop().add(hop_2);

        //Por defecto 1
        Recipe recipe_1 = new Recipe("Franciskaner", "1", 15.2, 4.4, 0.48, 90);
        Hop hop_4 = new Hop("Annauir, Valencia", recipe_1.getName(), "España", "Da un toque dulce a la cerveza.", "Gusto", "1000cl", 9, 3.00, 90.00);
        recipe_1.getListHop().add(hop_4);

        //Por defecto 2
        Recipe recipe_2 = new Recipe("Turia", "2", 7.7, 4.0, 1.48, 100);
        Hop hop_5 = new Hop("Massalaves, Valencia", recipe_2.getName(), "España", "Da un toque agridulce.", "Gusto", "4000cl", 10, 43.00, 900.00);
        Hop hop_6 = new Hop("Benetuser, Valencia", recipe_2.getName(), "España", "Da un color más claro a la cerveza.", "Visual", "200cl", 12, 22.00, 22.00);
        recipe_2.getListHop().add(hop_5);
        recipe_2.getListHop().add(hop_6);

        recipes.add(recipe_0);
        recipes.add(recipe_1);
        recipes.add(recipe_2);

        return recipes;
    }
}
