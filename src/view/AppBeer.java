/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.DAO.BeerDAO;
import controller.DAO.ConnectionDB;
import controller.parsers.ControllerDom;
import controller.parsers.ControllerRecipes;
import controller.DAO.HopDAO;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.Scanner;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import model.Constants;
import model.Hop;
import model.Recipe;
import model.Recipes;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 *
 * @author toure19
 */
public class AppBeer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException, Exception {
        Scanner s = new Scanner(System.in);
        Document doc = null, docNew = null;
        ControllerRecipes cr = new ControllerRecipes();
        ControllerDom cd = new ControllerDom();
        Recipes recipes = null;
        Recipes recipesNew = null;
        BeerDAO beerDao = null;
        HopDAO hopDao;
        Hop hop;
        ConnectionDB conexion_db = null;
        Connection con = null;
        Recipe rec = null;
        int op;

        do {
            showMenu();
            op = s.nextInt();
            s.nextLine();
            switch (op) {
                case 1:
                    doc = cr.recove(new File(Constants.XML));
                    break;
                case 2:
                    recipes = new Recipes();
                    recipes = cr.readRecipes(doc);
                    break;
                case 3:
                    recipes.printRecipes();
                    break;
                case 4:
                    recipesNew = new Recipes();
                    docNew = cd.instanceDocument(docNew);
                    cr.createRecipesDefault(recipesNew);
                    break;
                case 5:
                    docNew = cr.writeRecipes(docNew, recipesNew);
                    break;
                case 6:
                    cr.save(docNew, new File(Constants.NEWFILEXML));
                    break;
                case 7: //Insertar RECIPE
                    beerDao = new BeerDAO();
                    conexion_db = new ConnectionDB();
                    con = conexion_db.abrirConexion();
                    rec = new Recipe("Mahou", "2.2", 3.00, 8.00, 7.00, 90);
                    beerDao.inserta(con, rec);
                    rec = new Recipe("Judas", "3.5", 33.00, 18.00, 73.00, 190);
                    beerDao.inserta(con, rec);
                    conexion_db.cerrarConexion(con);
                    break;
                case 8: //Eliminar RECIPE
                    beerDao = new BeerDAO();
                    conexion_db = new ConnectionDB();
                    con = conexion_db.abrirConexion();
                    rec = new Recipe();
                    rec.setName("Judas");
                    beerDao.elimina(con, rec);
                    conexion_db.cerrarConexion(con);
                    break;
                case 9: //Actualizar RECIPE
                    beerDao = new BeerDAO();
                    conexion_db = new ConnectionDB();
                    con = conexion_db.abrirConexion();
                    rec = new Recipe("Mahou", "7.5", 7.00, 18.00, 77.00, 120);
                    beerDao.actualiza(con, rec);
                    conexion_db.cerrarConexion(con);
                    break;
                case 10: //Buscar RECIPE
                    beerDao = new BeerDAO();

                    conexion_db = new ConnectionDB();
                    con = conexion_db.abrirConexion();
                    rec = beerDao.findByName(con, "Mahou");
                    System.out.println(rec);
                    conexion_db.cerrarConexion(con);
                    break;
                case 11: //Insertar HOP
                    beerDao = new BeerDAO();
                    hopDao = new HopDAO();
                    conexion_db = new ConnectionDB();
                    con = conexion_db.abrirConexion();
                    rec = beerDao.findByName(con, "Mahou");
                    hop = new Hop("Hervir cebada", rec.getName(), "Xàtiva", "Hervir a fuego lento para incrementar el aroma.", "rubia", "Jose Francisco", 2, 2.2, 90.00);
                    hopDao.inserta(con, hop);
                    conexion_db.cerrarConexion(con);
                    break;
                case 12: //Eliminar HOP
                    hopDao = new HopDAO();
                    conexion_db = new ConnectionDB();
                    con = conexion_db.abrirConexion();
                    hop = new Hop();
                    hop.setName("Hervir cebada");
                    hopDao.elimina(con, hop);
                    conexion_db.cerrarConexion(con);
                    break;
                case 13: //Actualizar HOP
                    beerDao = new BeerDAO();
                    hopDao = new HopDAO();
                    conexion_db = new ConnectionDB();
                    con = conexion_db.abrirConexion();
                    rec = beerDao.findByName(con, "Mahou");
                    hop = new Hop("Hervir cebada", rec.getName(), "Sorio", "Hervir a fuego lento para incrementar el aroma.", "negra", "Pepe Paco", 22, 3.1, 120.00);
                    hopDao.actualiza(con, hop);
                    conexion_db.cerrarConexion(con);
                    break;
                case 14: //Buscar HOP
                    hopDao = new HopDAO();
                    conexion_db = new ConnectionDB();
                    con = conexion_db.abrirConexion();
                    hop = hopDao.findByName(con, "Hervir cebada");
                    System.out.println(hop);
                    conexion_db.cerrarConexion(con);
                    break;
                case 15: //Recuperamos los datos del xml y insertamos en la BBDD
                    doc = cr.recove(new File(Constants.XML));
                    recipes = new Recipes();
                    recipes = cr.readRecipes(doc);

                    beerDao = new BeerDAO();
                    hopDao = new HopDAO();
                    conexion_db = new ConnectionDB();
                    con = conexion_db.abrirConexion();
                    for (Recipe re : recipes) {
                        beerDao.inserta(con, re); //Insertamos recipe
                        for (Hop ho : re.getListHop()) {
                            hopDao.inserta(con, ho); //Insertamos hop
                        }
                    }
                    conexion_db.cerrarConexion(con);
                    break;
                case 0:
                    System.out.println("Saliendo.");
                    break;
                default:
                    System.out.println("Opción incorrecta.");
                    break;
            }

        } while (op != 0);

    }

    public static void showMenu() {
        System.out.println("1. Recuperar XML.");
        System.out.println("2. Leer XML.");
        System.out.println("3. Mostrar recetas.");
        System.out.println("4. Crear un objeto Recipes y Recipe por defecto.");
        System.out.println("5. Escribir el objeto Recipes en un nuevo documento.");
        System.out.println("6. Guardar documento en XML.");
        System.out.println("--------");
        System.out.println("PASO A PASO");
        System.out.println("7. Insertar recipe.");
        System.out.println("8. Eliminar recipe.");
        System.out.println("9. Actualizar recipe.");
        System.out.println("10. Buscar recipe");
        System.out.println("11 Insertar hop.");
        System.out.println("12. Eliminar hop");
        System.out.println("13. Actualizar hop.");
        System.out.println("14. Buscar hop.");
        System.out.println("--------");
        System.out.println("TODO DE GOLPE");
        System.out.println("15. Insertar datos recuperados del XML.");
        System.out.println("0. Salir.");
    }
}
