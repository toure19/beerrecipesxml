/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author toure19
 */
public class Recipe {

    private String name;
    private String version;
    private Double batchSize;
    private Double boilSize;
    private Double efficiency;
    private int boilTime;
    private ArrayList<Hop> listHop = new ArrayList<Hop>();

    public Recipe() {
    }

    public Recipe(String name, String version, Double batchSize, Double boilSize, Double efficiency, int boilTime) {
        this.name = name;
        this.version = version;
        this.batchSize = batchSize;
        this.boilSize = boilSize;
        this.efficiency = efficiency;
        this.boilTime = boilTime;
    }

    @Override
    public String toString() {
        return "Recipe{" + "name=" + name + ", version=" + version + ", batchSize=" + batchSize + ", boilSize=" + boilSize + ", efficiency=" + efficiency + ", boilTime=" + boilTime + ", listHop=" + listHop + '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Double getBatchSize() {
        return batchSize;
    }

    public void setBatchSize(Double batchSize) {
        this.batchSize = batchSize;
    }

    public Double getBoilSize() {
        return boilSize;
    }

    public void setBoilSize(Double boilSize) {
        this.boilSize = boilSize;
    }

    public Double getEfficiency() {
        return efficiency;
    }

    public void setEfficiency(Double efficiency) {
        this.efficiency = efficiency;
    }

    public int getBoilTime() {
        return boilTime;
    }

    public void setBoilTime(int boilTime) {
        this.boilTime = boilTime;
    }

    public ArrayList<Hop> getListHop() {
        return listHop;
    }

    public void setListHop(ArrayList<Hop> listHop) {
        this.listHop = listHop;
    }

}
