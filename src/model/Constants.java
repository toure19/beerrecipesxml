/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author toure19
 */
public interface Constants {

    static final String RECIPES = "RECIPES";
    static final String RECIPE = "RECIPE";
    static final String NAME = "NAME";
    static final String ORIGIN = "ORIGIN";
    static final String NOTES = "NOTES";
    static final String TYPE = "TYPE";
    static final String INVENTORY = "INVENTORY";
    static final String VERSION = "VERSION";
    static final String AMOUNT = "AMOUNT";
    static final String TIME = "TIME";
    static final String HOPS = "HOPS";
    static final String HOP = "HOP";
    static final String RECIPENAME = "RECIPE_NAME";
    static final String BREWER = "BREWER";
    static final String BATCHSIZE = "BATCH_SIZE";
    static final String BOILSIZE = "BOIL_SIZE";
    static final String EFFICIENCY = "EFFICIENCY";
    static final String BOILTIME = "BOIL_TIME";
    static final String NEWFILEXML = "newRecipes.xml";
    static final String XML = "recipes.xml";

}
