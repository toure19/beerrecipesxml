/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author toure19
 */
public class Hop {

    private String name;
    private String recipeName;
    private String origin;
    private String notes;
    private String type;
    private String inventory;
    private int version;
    private double amount;
    private double time;

    public Hop() {
    }

    public Hop(String name, String recipeName, String origin, String notes, String type, String inventory, int version, double amount, double time) {
        this.name = name;
        this.recipeName = recipeName;
        this.origin = origin;
        this.notes = notes;
        this.type = type;
        this.inventory = inventory;
        this.version = version;
        this.amount = amount;
        this.time = time;
    }

    @Override
    public String toString() {
        return "Hop{" + "name=" + name + ", recipeName=" + recipeName + ", origin=" + origin + ", notes=" + notes + ", type=" + type + ", inventory=" + inventory + ", version=" + version + ", amount=" + amount + ", time=" + time + '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRecipeName() {
        return recipeName;
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getInventory() {
        return inventory;
    }

    public void setInventory(String inventory) {
        this.inventory = inventory;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

}
